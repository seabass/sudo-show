// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

package main

import (
	"context"
	"fmt"
	"golang.org/x/term"
	"io"
	"net/http"
	"os"
	"os/exec"
	"time"

	"sudo-show/feed"
	"sudo-show/progress"

	"github.com/fatih/color"
	ioProgress "github.com/machinebox/progress"
)

func quitWithErrorMessage(message string) {
	brightRedColour := color.New(color.FgRed).Add(color.Bold)
	brightRedColour.Print("Error! ")
	fmt.Println(message)
	os.Exit(1)
}

func main() {
	const FEED_URL string = "https://feeds.fireside.fm/sudoshow/rss"
	termWidth, _, _ := term.GetSize(int(os.Stdin.Fd()))
	var err error
	brightRedColour := color.New(color.FgRed).Add(color.Bold)
	brightGreenColour := color.New(color.FgGreen).Add(color.Bold)

	item, err := feed.GetLatestFeedItem(FEED_URL)
	if err != nil {
		quitWithErrorMessage("Couldn't fetch latest episode from feed.")
	}
	brightRedColour.Println(item.Title)

	HttpResponse, err := http.Get(item.Enclosures[0].URL)
	if err != nil {
		quitWithErrorMessage("Couldn't get URL of latest episode.")
	}
	bodySize := HttpResponse.ContentLength
	defer HttpResponse.Body.Close()

	file, err := os.Create("/tmp/sudo-show.mp3")
	if err != nil {
		quitWithErrorMessage("Couldn't open /tmp/sudo-show.mp3 for writing.")
	}
	defer file.Close()

	ctx, cancel := context.WithCancel(context.Background())
	body := ioProgress.NewReader(HttpResponse.Body)
	go func() {
		progressChannel := ioProgress.NewTicker(ctx, body, bodySize, 40*time.Millisecond)
		for i := range progressChannel {
			progress.PrintProgress(termWidth, i.Percent()/100)
		}
	}()

	_, err = io.Copy(file, body)
	if err != nil {
		quitWithErrorMessage("Couldn't download episode.")
	}
	cancel()
	brightGreenColour.Println("\nDownload complete!")

	mediaPlayerCommand := exec.Command("xdg-open", "/tmp/sudo-show.mp3")
	err = mediaPlayerCommand.Run()
	if err != nil {
		quitWithErrorMessage("Couldn't play downloaded episode.")
	}
}
