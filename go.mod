// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

module sudo-show

go 1.15

require (
	github.com/SlyMarbo/rss v1.0.1
	github.com/fatih/color v1.13.0
	github.com/machinebox/progress v0.2.0
	github.com/matryer/is v1.4.0 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
)
