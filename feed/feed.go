// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

package feed

import "github.com/SlyMarbo/rss"

func GetLatestFeedItem(feedURL string) (*rss.Item, error) {
	feed, err := rss.Fetch(feedURL)
	item := feed.Items[0]
	return item, err
}
