// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

package progress

import (
	"fmt"
	"github.com/fatih/color"
	"math"
)

func PrintProgress(width int, progress float64) {
	const barChar = "="
	const endChar = "#"
	const cursorStart = "\x0d"
	const clearLine = "\x1b[2K"
	const startEdge = "["
	const endEdge = "]"
	const blurb = "Downloading... "

	spaceForBlocks := width - len(blurb) - 8
	blocks := int(math.Ceil(progress * float64(spaceForBlocks)))
	brightYellowColour := color.New(color.FgYellow).Add(color.Bold)
	brightBlueColour := color.New(color.FgBlue).Add(color.Bold)

	fmt.Print(clearLine, cursorStart)
	brightYellowColour.Print(blurb)
	fmt.Print(color.YellowString(startEdge))

	for i := 0; i <= spaceForBlocks; i++ {
		switch {
		case i < blocks:
			fmt.Print(color.CyanString(barChar))
		case i == blocks:
			brightBlueColour.Print(endChar)
		default:
			fmt.Print("-")
		}
	}

	fmt.Print(color.YellowString(endEdge))
	brightYellowColour.Printf(" %3.0f%%", progress*100)
}
