<!--- SPDX-License-Identifier: CC-BY-SA-4.0 -->
<!--- SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com> -->

[![REUSE status](https://api.reuse.software/badge/codeberg.org/seabass/sudo-show)](https://api.reuse.software/info/codeberg.org/seabass/sudo-show)
[![CI status](https://img.shields.io/drone/build/seabass/sudo-show?label=tests&server=https%3A%2F%2Fci.codeberg.org)](https://ci.codeberg.org/seabass/sudo-show)

# sudo-show

🛡️ Listen to the latest episode of the [Sudo Show](https://sudo.show/) with a single command!

[![Demonstration video](https://asciinema.org/a/455464.svg)](https://asciinema.org/a/455464)

`sudo-show` is an unofficial program which does the hard work of parsing RSS feeds and downloading MP3 files, all so that you can enjoy the podcast whilst expending literally no effort! 😀
It's written in the Go programming language, which allows it to compile down to a single small binary.
`sudo-show` was inspired by a joke on the Sudo Show chatroom, so please don't take it too seriously.
That said, [pull requests](https://codeberg.org/seabass/sudo-show/pulls) and [issue reports](https://codeberg.org/seabass/sudo-show/issues) are more than welcome.

## Building from source

You'll need the Go compiler installed on your computer to build `sudo-show` from source.

```bash
git clone https://codeberg.org/seabass/sudo-show
cd sudo-show
go get
go build
./sudo-show
```

## Licensing

Of the files in this repository, most are made available under the Apache-2.0 licence and others are under the CC-BY-SA-4.0 licence.
This repository is [REUSE-compliant](https://reuse.software/), so you can simply look inside a file to find out which licence and copyright applies to it, and you can use the [REUSE Helper Tool](https://reuse.readthedocs.io/en/stable/readme.html) to automate even that!

Contributions are most welcome!
Please add your own copyright notice to files you change.
Also, please add a `Signed-off-by` line to your commit messages to show that you agree to the [Developer Certificate of Origin, version 1.1](https://developercertificate.org/).
